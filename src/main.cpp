/**
 * @file main.cpp
 * @brief Template code for building with Shunya Stack
 * 
 * Compilation: Code comes with a cmake file, just run cmake 
 * 
 * Usage : Just run the command './main'
 */

/* --- Standard Includes --- */
#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <time.h> 
#include <unistd.h>
#include <errno.h>

#include <opencv2/opencv.hpp>

/* --- RapidJSON Includes --- */
/* MANDATORY: Allows to parse Shunya AI binaries output */
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/filereadstream.h"

#include "subprocess.hpp" /* MANDATORY: Allows to run Shunya AI binaries */
#include "exutils.h" /* MANDATORY: Allows to parse Shunya AI binaries output */


/* --- Shunya Interfaces Includes --- */
#include <si/shunyaInterfaces.h> /* MANDATORY: Contains all IoT Functions */
#include <si/video.h>
#include <si/whatsapp.h> 

using namespace std;
using namespace rapidjson;



int main(void)
{
    /* MANDATORY: Initializes the Shunya components */
    initLib();


    /* --- Capturing image from video using Video component --- */
    while(true){

    
    captureObj src = newCaptureDevice("camera"); /* Create capture Instance */
    cv::Mat inputImage;
    int32_t outIndex = 0;

    /*################## Call Video Component functions ################*/
    inputImage  = captureFrameToMem(&src); /* Capture one frame at a time in a loop*/

    if (inputImage.empty()) {
        fprintf(stderr, "End of video file!.");
        closeCapture(&src);
        return 0;
    }
}
    /* ---- Create cv::mat image to base64 string ---- */
    std::string b64InpImg = mat2Base64(inputImage);
    /* Set value to 0.8 i.e (80/100) to set the probability to 80% or higher */
    float probability = 0.8;  
    /* ---- Create Input JSON ---- */
        rapidjson::Document inputJson;
        inputJson.SetObject();
        rapidjson::Value inpImage;

        /* Call API binary */
        inpImage.SetString(b64InpImg.c_str(), strlen(b64InpImg.c_str()), inputJson.GetAllocator());
        inputJson.AddMember("inputImage", inpImage, inputJson.GetAllocator());
        inputJson.AddMember("probability", probability, inputJson.GetAllocator());
        /*############### Call detectObjects Component ##################*/
        subprocess::popen detectObjects("/usr/bin/detectObjects", {});
        detectObjects.stdin() << jsonDoc2Text(inputJson) << std::endl;
        detectObjects.close();
        std::string detectObjectsOut;
        detectObjects.stdout() >> detectObjectsOut;

        /* ---- Printing detected objects information ---- */
    rapidjson::Document detectObjectsJson = readJsonString(detectObjectsOut);
    if (detectObjectsJson.HasMember("data")) {
        rapidjson::Value &results = detectObjectsJson["data"]["objects"];
        assert(results.IsArray());
        /* Reading from json file and add it in the structure */
        for (rapidjson::SizeType i = 0; i < results.Size(); i++) {
            // Printing detected objects values
            std::cout<<"\nData:";
            std::cout<<"\nObject"<<std::to_string(i)<<": ";
            std::cout<<"\nObject Label: "<<results[i]["object"].GetString();
            std::cout<<"\nObject probability: "<<results[i]["confidence"].GetFloat();
        }
    }
    else{
        std::cout<<"\nno json member as data ";
    }

    

    
    


    return 0;
}
