## Project Template

# No Trespass Area

The project intends to set off an alarm if any person was detected trespassing the area. `(Describe your project in short)`

![project-image](extra/vms_stock_res_1280x720.jpg)

## Shunya Stack `(Since the project is built using Shunya’s stack, you are adding this part to give credits similar to how we did for the image above)`
Project is built by using the Shunya stack.

-   ShunyaOS is a lightweight Operating system with built-in support for AI and IOT.
-   Shunya Stack Low Code platform to build AI, IoT and AIoT products, that allows you to rapidly create and deploy AIoT products with ease.

For more information on ShunyaOS see : http://demo.shunyaos.org


## Documentation `(Add your project documentation link after done)`
For developers see detailed Documentation on the components of the project in the [Wiki]()

## Project Overview `(Update after your project excel is created)`

1.  [Project Plan Excel]()
1.  [Get a birds-eye status of the project]( )


## Contributing
Help us improve the project.

Ways you can help:

1.  Choose from the existing issue and work on those issues.
2.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
3.  Find a bug create an Issue and report it.
4.  Review Issues or Merge Requests, give the developers the feedback.
5.  Fix Documentation.

## Contributors 

#### Team Lead `(Team member names here)`
1. Varunavi Shettigar (@varunavi)

#### Active Contributors.

1.  Darshan Nere - @darshan.nere
2.  Lokesh Kasliwal - @lkasliwal 
3.  Priyal Mohod - @priyalnm2001
